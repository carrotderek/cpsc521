
{- |
This is for our purposes now, a data type that could be used to represent the terms of our
language, BCPL.  Later it we will expand it to a parser so that we have a canonical mapping
from strings into this data type.

The terms are all in non-paired form for ease of manipulation, but note that cons (for example) is
(a,SimpleLambda a) in the specification handout.
-}

--import Parsec
module SimplyTypedParser where

{- |
Data type for representing the terms of the simply typed lambda calculus with fixed points and extra programming
features to make life easier and more programmable and my old english prof. would kill me if she saw this sentence.
-}
data SimpleLambda a =
    -- | a is the variable name for what is projected into.  These are just variables.
      Proj a
    -- | Abst (bound variable) (lambda term)
    | Abst a (SimpleLambda a)
    -- | App (lambda term) (lambda term)
    | App (SimpleLambda a) (SimpleLambda a)
    -- | Fix (a recursive signature, shorthand for Y)
    | Fix (SimpleLambda a)
    -- | Pair a b <=> (a,b)
    | Pair (SimpleLambda a) (SimpleLambda a)
    -- | Paircase (lambda expression to match on) (bound variable to assoicative first elt of pair) ("..." second elt of pair) (term or expression the pair maps to)
    | PairCase (SimpleLambda a) a a (SimpleLambda a)
    -- | Nullary constructor, used to pattern match on for empty computations
    | Unit
    -- | UnitCase (term reducing to unit) (term or expression to map to)
    | UnitCase (SimpleLambda a) (SimpleLambda a)
    -- | Zero + Zero = Zero or Everyday is wednesday
    | Zero
    -- | Succ (A natural number (hopefully))
    | Succ (SimpleLambda a)
    -- | NatCase (Number to match on) (term that zero maps to) (bound variable to associate n, in succ n to) (term or expression that succ n maps to)
    | NatCase (SimpleLambda a) (SimpleLambda a) a (SimpleLambda a)
    -- | Base case for lists.  Note unit, zero, nil have same form, but are used differently in type inference.
    | Nil
    -- | Cons (bound variable to associate the (new) head of a list) (list (hopefully))
    | Cons a (SimpleLambda a)
    -- | ListCase (expression to match on) (term that nil maps to) (bound variable to associate x to in (x:xs)) (term that (x:xs) maps to)
    | ListCase (SimpleLambda a) (SimpleLambda a) a (SimpleLambda a)

-- | No comment
-- | Is the above a contradiction (if this sentence weren't written)?
foldSimpleLambda ::
    (a -> b)                ->
    (a -> b -> b)           ->
    (b -> b -> b)           ->
    (b -> b)                ->
    (b -> b -> b)           ->
    (b -> a -> a -> b -> b) ->
    b                       ->
    (b -> b -> b)           ->
    b                       ->
    (b -> b)                ->
    (b -> b -> a -> b -> b) ->
    b                       ->
    (a -> b -> b)           ->
    (b -> b -> a -> b -> b) ->
    SimpleLambda a          ->
    b
foldSimpleLambda
    prj
    abs
    app
    fix
    pr
    prcse
    unt
    untcse
    zro
    sc
    ntcse
    nil
    cons
    lstcse
    term =
        case term of
            Proj a -> prj a
            Abst a t -> abs a (foldSimpleLambda' t)
            App m n -> app (foldSimpleLambda' m) (foldSimpleLambda' n)
            Fix m -> fix (foldSimpleLambda' m)
            Pair a b -> pr (foldSimpleLambda' a) (foldSimpleLambda' b)
            PairCase t a b s -> prcse (foldSimpleLambda' t) a b (foldSimpleLambda' s)
            Unit -> unt
            UnitCase t s -> untcse (foldSimpleLambda' t) (foldSimpleLambda' s)
            Zero -> zro
            Succ n -> sc (foldSimpleLambda' n)
            NatCase t n0 n n1 -> ntcse (foldSimpleLambda' t) (foldSimpleLambda' n0) n (foldSimpleLambda' n1)
            Nil -> nil
            Cons x xs -> cons x (foldSimpleLambda' xs)
            ListCase t nl x s -> lstcse (foldSimpleLambda' t) (foldSimpleLambda' nl) x (foldSimpleLambda' s)
            where
                foldSimpleLambda' = foldSimpleLambda prj abs app fix pr prcse unt untcse zro sc ntcse nil cons lstcse

-- | The world's greatest functions.  What are some free theorems for this datatype?
mapSimpleLambda :: (a -> b) -> SimpleLambda a -> SimpleLambda b
mapSimpleLambda f =
    foldSimpleLambda
        (Proj . f)
        (Abst . f)
        App
        Fix
        Pair
        (\t a b s -> PairCase t (f a) (f b) s)
        Unit
        UnitCase
        Zero
        Succ
        (\t n0 n n1 -> NatCase t n0 (f n) n1)
        Nil
        (Cons . f)
        (\t nl x s -> ListCase t nl (f x) s)
