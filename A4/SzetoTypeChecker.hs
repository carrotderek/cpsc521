-- Derek Szeto
-- 315083
-- CPSC521 Assignment 4: Type Checker for the extended simply typed lambda calculus
---------------------------------

module SzetoTypeChecker where

-- referencing parser and unification algorithm - provided by Jonathan Gallagher
import Unification
import SimplyTypedParser
import List

-------------------------------------------------------------------------------
-- Pretty printer for lambda terms etc
-------------------------------------------------------------------------------
instance (Show a) => Show (SimpleLambda a) where
	show (Proj a) = show a
	show (Abst a l ) =  "\\" ++ (show a) ++ "." ++ (show l) ++ ""
	show (App l1 l2) = "(" ++ (show l1) ++ " " ++ (show l2) ++ ")"
	show (Fix l) = "Y [" ++ (show l) ++ "]"
	show (Pair a b) = "(" ++ (show a) ++ ", " ++ (show b) ++ ")"
	show (PairCase t x y s) = "case " ++ (show t) ++ " of (" ++ (show x) ++ "," ++ (show y) ++ ") -> " ++ (show s)
	show Unit = "()"
	show (UnitCase t s) =  "case " ++ (show t) ++ " of () -> " ++ (show s)
	show Zero = "0"
	show (Succ l) = "Succ " ++ (show l) --er..
	show (NatCase t t0 n t1) = "case " ++ (show t) ++ " of zero -> " ++ (show t0) ++ ", succ " ++ (show n) ++ " -> " ++ (show t1)
	show Nil = "Nil"
	show (Cons n l) = (show n) ++ ":" ++ (show l)
	show (ListCase t t0 v t1) = "case " ++ (show t) ++ " of nil ->" ++ (show t0) ++ ", cons " ++ (show v) ++ " -> " ++ (show t1)

-------------------------------------------------------------------------------
-- Type equation collector
-- need to collect the number of type equations into a list
-- takes in an a context gamma, current element, a simplelambda datatype
-------------------------------------------------------------------------------
collect_eq :: [(Expr f v)] -> SimpleLambda a -> [(Expr f v)]
collect_eq gamma (Proj a) = undefined

--------------------------------------------------------------------------------
-- Determining free variables in a term 
-- Free variable cases
-- free(x) = {x}
-- free(\x.t) = free(t) - {x}
-- free(t1 t2) = free(t1) U free(t2)
--------------------------------------------------------------------------------
-- TEST CASE(S):
---------------------------------------------------------------
-- freevars (Var 1)
-- Expected Output: [1]
---------------------------------------------------------------
-- for (\x.x)
-- freevars (Abst 1 (Var 1))
-- Expected Output: []
---------------------------------------------------------------
-- for (\x.y)(x)
-- freevars (Apply (Abst 1 (Var 2)) (Var 1))
-- Expected Output: [2, 1]
---------------------------------------------------------------
-- for (\x.yx)(\z.z)
-- freevars (Apply (Abst 1 (Apply (Var 2) (Var 1))) (Abst 3 (Var 3)))
-- Expected Output: [2]
--------------------------------------------------------------------------------
freevars :: (Eq a) => (SimpleLambda a) -> [a]
freevars (Proj a) = [a]
freevars (Abst a t) = (freevars t) \\ [a]
freevars (App t1 t2) = union (freevars t1) (freevars t2)

--------------------------------------------------------------------------------
-- Getting fresh variables when performing an alpha conversion
-- Given a list of free variables, we need to pick one that is not in that list
-- In this case, just give the successor of the highest value in the given list
--------------------------------------------------------------------------------
-- TEST CASE(S):
---------------------------------------------------------------
-- freshvar [2]
-- Expected Output: 3
---------------------------------------------------------------
-- freshvar [2,4,1,3]
-- Expected Output: 5
--------------------------------------------------------------------------------
freshvar :: (Enum a, Ord a) => [a] -> a
freshvar xs = succ $ head $ reverse $ sort xs

--------------------------------------------------------------------------------
-- Performing subsitution
-- Substitution cases
-- (\x.t)[L/y]  = (\x.t) if x==y
--			    = (\x.t) if y not elem of t
--			    = (\f.(t[f/x])[L/y]) if x is elem of L, f = new fresh variable
--			    = (\x.t[L/y]) otherwise
--------------------------------------------------------------------------------
-- TEST CASE(S):
---------------------------------------------------------------
-- lamSubst (Proj 1) ((Proj 2), 2)
-- Expected Output: 1
---------------------------------------------------------------
-- lamSubst (Proj 1) ((Proj 2), 1)
-- Expected Output: 2
---------------------------------------------------------------
-- lamSubst (Abst 3 (Proj 1)) ((Proj 2), 3)
-- Expected Output: \3.1
---------------------------------------------------------------
-- lamSubst (Abst 3 (Proj 1)) ((Proj 2), 2)
-- Expected Output: \3.1
---------------------------------------------------------------
-- variable capture ( \x.y [x/y])
-- subst (Abst 1 (Proj 2)) ((Proj 1), 2)
-- Expected Output: \3.1
---------------------------------------------------------------
-- lamSubst (Abst 1 (Proj 2)) ((Proj 5), 2)
-- Expected Output: \1.5
---------------------------------------------------------------
-- (\x.y)(\x.y)[w/y]
-- lamSubst (Apply (Abst 1 (Proj 2)) (Abst 1 (Proj 2)) ) ((Proj 4), 2)
-- Expected Output: \1.4\1.4
--------------------------------------------------------------------------------
lamSubst :: (Enum a, Ord a) => SimpleLambda a -> (SimpleLambda a, a) -> SimpleLambda a
lamSubst (Proj x) (l, y)
	| x == y						= l
	| otherwise						= Proj x
lamSubst (Abst x t) (l, y)
	| x == y						= Abst x t
	| not (elem y (freevars t))		= Abst x t
	| elem x (freevars l)			= Abst f (lamSubst (lamSubst t ((Proj f), x)) (l, y))
	| otherwise						= Abst x (lamSubst t (l, y)) where
		f = freshvar (freevars t)

-------------------------------------------------------------------------------
-- performs substitution of PairCase in one step
-- case N of (x,y) -> M ~>R
-- N ~>(N1,N2) M[N1/x, N2/y] ~> R
-- takes in the lambda term to reduce, the pair to substitute, and returns the new lambda term with substituted
-------------------------------------------------------------------------------
--pairSubst :: (Enum a, Ord a) => SimpleLambda a -> (a,a) -> (a,a) -> SimpleLambda a
--pairSubst l (a,b) (x,y) = (lamSubst l (a, x)) 

-------------------------------------------------------------------------------
-- reduces redexes for BPL
-- The boolean tells us if this lambda term can be reduced further. 
-- True if it can, False if it cannot
-------------------------------------------------------------------------------
-- TEST CASE(S):
---------------------------------------------------------------
-- bplReduc ((App (Abst 1 (Proj 1)) (Proj 3)), False)
-- expected result: (3, True)
bplReduc :: (Enum a, Ord a) => (SimpleLambda a, Bool) -> (SimpleLambda a, Bool)
bplReduc (lambda, True)							= (lambda, True)
bplReduc (Proj a, False)						= (Proj a, False)
bplReduc (Abst a l, False)  					= (Abst a l, False) -- \x.N -> \x.N
bplReduc (App (Abst a l1) l2, False)			= (lamSubst l1 (l2, a), True) -- N -> \x.N' N'[M/x] -> R
bplReduc (App l1 l2, False)						= (App l1 l2, False) -- (N,M) -> (N,M)
bplReduc (Fix l, False)							= ((App l (Fix l)), True) -- M fix[M] -> R
bplReduc (PairCase (Pair a b) x y s, False)		= (lamSubst (lamSubst s (a, x)) (b, y), True) -- N -> (N1, N2), M[N1/x, N2/y] -> R
bplReduc (NatCase Zero t0 n t1, False)			= (t0, False) -- N -> Zero, t0 -> R
bplReduc (NatCase (Succ n') t0 n t1, False)		= (lamSubst t1 (n', n), True) -- N->succ N' M1[N'/n] -> R
bplReduc (ListCase Nil t0 n t1, False)			= (t0, False) -- L -> Nil, t0 -> R 
bplReduc (ListCase (Cons n l) t0 z t1, False)	= (lamSubst t1 (l, n), True) -- N-> cons N' M1[N'/z] -> R
bplReduc (Zero, False)							= (Zero, False)
bplReduc (Nil, False)							= (Nil, False)
bplReduc (Succ n, False)						= (Succ n, False)
bplReduc (Cons n l, False)						= (Cons n l, False)
bplReduc (lambda, False)						= (lambda, True)

-------------------------------------------------------------------------------
-- performs a normal-order reduction on the term
-- if it can be reduced, it will recurse on the term returned by one-step function
-- around the same method used by third assignment
-------------------------------------------------------------------------------
normalize :: (Enum a, Ord a) => SimpleLambda a -> SimpleLambda a
normalize lambda | canReduce = normalize newLambda
						| otherwise = newLambda where
					(newLambda, canReduce) = bplReduc (lambda, False)
 