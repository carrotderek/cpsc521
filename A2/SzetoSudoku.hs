------------------------------------
-- Derek Szeto
-- 315083
-- CPSC521 Assignment 2 - Sudoku Solver
-------------------------------------
-- Referenced from http://www.cs.nott.ac.uk/~gmh/sudoku.lhs


module SzetoSudoku where
	
import List
--------------------------------------------------------------------------
-- TEST BOARDS
--------------------------------------------------------------------------
-- exampleBoard2x2
exampleBoard2x2 :: [[[[Int]]]]
exampleBoard2x2 = 
	[
		[
			[
				[0,0],
				[0,0]
			],
			
			[
				[4,3],
				[2,1]
			]
		],
		
		[
			[
				[2,1],
				[4,3]
			],
			
			[
				[0,4],
				[1,2]
			]
		]
	]

simple :: [[[[Int]]]]
simple = [
			[
				[
					[0,1],
					[4,0]
				]
			]
		 ]
		
board2 :: [[[[Int]]]]
board2 = 
			[
				[
					[
						[1,2],
						[3,4]
					],

					[
						[3,4],
						[2,1]
					]
				],

				[
					[
						[4,3],
						[2,1]
					],

					[
						[1,2],
						[4,3]
					]
				]
			]

failboard :: [[[[Int]]]]
failboard = 

			[
					[
						[
							[50,5],
							[33,21]
						],

						[
							[3,4],
							[2,1]
						]
					],

					[
						[
							[4,3],
							[2,1]
						],

						[
							[1,2],
							[4,3]
						]
					]
				]
--------------------------------------------------------------------------
-- Data Types
--------------------------------------------------------------------------
type Matrix a = [[a]]

type SudokuBoard a = Matrix (Matrix a)   -- board is a list of boxes

-- represent the board as a SudokuBoard of lists of values
-- to deal with possbilities
type NonDetermBoard a = SudokuBoard [a]

--------------------------------------------------------------------------
-- HELPER FUNCTIONS
--------------------------------------------------------------------------
-- this is to get all possible values for a cell on the board
-- takes in a board and will return a list of integers according to boardsize
--------------------------------------------------------------------------
-- Test cases:
--   values [[[[1,2],[3,4]],[[4,3],[2,1]]],[[[2,1],[4,3]],[[3,4],[1,2]]]]
--   expected output: [1,2,3,4]
--------------------------------------------------------------------------
values :: Int-> [Int]
values boardSize = [1..n] where n = (boardSize)^2

-- we will need this when we look for blank squares
-- this will return True if the value is blank (zero)
empty :: Int -> Bool
empty = (== 0)

-- a helper function to define what entails a singleton
-- in which it is a list with length 1
singleton :: [a] -> Bool
singleton x = length x == 1

-- a helper function for getting the length (n)of a Sudoku board (nxn)
--------------------------------------------------------------------------
-- test case:
--   getBdSize [[[[1,2],[3,4]],[[4,3],[2,1]]],[[[2,1],[4,3]],[[3,4],[1,2]]]]
--   expected output: 2
--------------------------------------------------------------------------
getBdSize :: SudokuBoard a -> Int
getBdSize =  length . head . head . head

-- given a board, this function will return a list of rows 
--------------------------------------------------------------------------
-- test cases:
--   rows [[[[1,2],[3,4]],[[4,3],[2,1]]],[[[2,1],[4,3]],[[3,4],[1,2]]]]
---  expected output: [[1,2,4,3],[3,4,2,1],[2,1,3,4],[4,3,1,2]]
--------------------------------------------------------------------------
rows :: SudokuBoard a -> [[a]]
rows =  map concat . concat . map transpose

-- given a board, this function will return a list of columns
--------------------------------------------------------------------------
-- test cases: 
--   columns [[[[1,2],[3,4]],[[4,3],[2,1]]],[[[2,1],[4,3]],[[3,4],[1,2]]]]
---  expected output: [[1,3,2,4],[2,4,1,3],[4,2,3,1],[3,1,4,2]]
--------------------------------------------------------------------------
columns :: SudokuBoard a -> [[a]]
columns = transpose . rows

-- given a board, this function will return a list of boxes
--------------------------------------------------------------------------
-- test cases: 
--   boxes [[[[1,2],[3,4]],[[4,3],[2,1]]],[[[2,1],[4,3]],[[3,4],[1,2]]]]
---  expected output: [[[1,2],[3,4]],[[4,3],[2,1]],[[2,1],[4,3]],[[3,4],[1,2]]]
--------------------------------------------------------------------------
boxes :: SudokuBoard a -> [[a]]
boxes = map concat . concat

-- a helper function map for sudoku boards
-- result is a board with values resulting from applying a the function (a->b)
--------------------------------------------------------------------------
-- test case:
--   mapSud (+3) [[[[1,2],[3,4]],[[4,3],[2,1]]],[[[2,1],[4,3]],[[3,4],[1,2]]]]
---  expected output: [[[[4,5],[6,7]],[[7,6],[5,4]]],[[[5,4],[7,6]],[[6,7],[4,5]]]]
--------------------------------
--   mapSud (negate) [[[[1,2],[3,4]],[[4,3],[2,1]]],[[[2,1],[4,3]],[[3,4],[1,2]]]]
--   expected output: [[[[-1,-2],[-3,-4]],[[-4,-3],[-2,-1]]],[[[-2,-1],[-4,-3]],[[-3,-4],[-1,-2]]]]
--------------------------------------------------------------------------
mapSud :: (a->b) -> SudokuBoard a -> SudokuBoard b
mapSud = map . map . map . map

-- helper function removes singletons from a list
--------------------------------------------------------------------------
-- Test case:
--   remove [1,2,3,4] [3,4]
--   expected output: [1,2]
--------------------------------------------------------------------------
remove :: (Eq a) => [a] -> [a] -> [a]
remove x y | singleton x = x
	       | otherwise = x \\ y

-- we need to prune the list, that is take out the singletons and leave in the possible values for blank squares
-- takes a list of rows, removes the singletons to get all non-definite values
--------------------------------------------------------------------------
-- Test case: 
--   reduce [[1,2,3,4], [1,2,3,4], [4], [3]]
--   expected output: [[1,2], [1,2], [4], [3]]
--------------------------------------------------------------------------
reduce :: (Eq a) => [[a]] -> [[a]]
reduce xss = [remove xs singletons | xs <- xss]
			 where singletons = concat(filter singleton xss)

--------------------------------------------------------------------------
-- SEARCH OUR SUDOKUBOARD
-- thanks Jon.
-- Searching is done by looking for non-singleton lists when traversing
-- the board levels from a row a matrices -> matrices -> rows -> elements
--------------------------------------------------------------------------

-- When at the elements, we look at these elements for lists that are not singletons
-- When there is a non-singleton list, return the coordinate of the list.
-- Otherwise, we keep searching.
--------------------------------------------------------------------------
-- Test cases:
--   findValue [[1,2],[1]] 0
--   expected output: 0
--------------------------------
--   findValue [[1],[1,2]] 0
--   expected output: 1
--------------------------------------------------------------------------			
findValue :: [[a]] -> Int -> Int
findValue (k:ks) q = if (not . singleton) k then q 
					 else findValue ks (q+1)
					
-- When at the at the level where it is a list of rows, we search for
-- a non-singleton list. Once that is found we return the coordinate for
-- which row it is on and which element it is.				
--------------------------------------------------------------------------
-- Test cases:
--   findRow [[[1,2],[2],[1]],[[1],[2,3]]] 0
--   expected output: (0,0)
--------------------------------
--   findRow [[[1],[2,2],[1]],[[1],[2,3]]] 0
--   expected output: (0,1)
--------------------------------------------------------------------------			
findRow :: [[[a]]] -> Int -> (Int, Int)
findRow (z:zs) p = if or (map (not . singleton) z) then (p, j) else findRow zs (p+1)
			where j = findValue z 0
			
-- When at the level where it is a list of matrices, we search the matrices
-- for a non-singleton list. Once that is found, we return the coordinates for
-- which matrix, which row, and which element it is located.			
--------------------------------------------------------------------------
-- Test cases:
--   findMatrix [[[[1,2,3],[1]]]] 0
--   expected output: (0,0,0)
--------------------------------
--   findMatrix [[[[1],[1,2,3]]]] 0
--   expected output: (0,0,1)
--------------------------------------------------------------------------			
findMatrix :: [[[[a]]]] -> Int -> (Int, Int, Int)
findMatrix (y:ys) m = if or (map (not . singleton) (concat y)) then (m,f,g) else findMatrix ys (m+1)
			where (f,g) = findRow y 0

-- When at the level where it is a list of rows of matrices (the board),
-- we search for a non-singleton list. Once that is found, we return the coordinates
-- for which row of matrices, which matrix, which row inside that matrix, and finally,
-- which element inside that row. 
--------------------------------------------------------------------------
-- Test cases:
--   findExpandPlace [[[[[1],[1,2,3,4]],[[1,2,3,4],[1,2,3,4]]],[[[4],[3]],[[2],[1]]]],[[[[2],[1]],[[4],[3]]],[[[1,2,3,4],[4]],[[1],[2]]]]] 0
--   expected output: (0,0,0,1)
--------------------------------
--   findExpandPlace [[[[[1,2,3,4],[1]],[[1,2,3,4],[1,2,3,4]]],[[[4],[3]],[[2],[1]]]],[[[[2],[1]],[[4],[3]]],[[[1,2,3,4],[4]],[[1],[2]]]]] 0
--   expected output: (0,0,0,0)
--------------------------------------------------------------------------			
findExpandPlace :: NonDetermBoard a -> Int -> (Int, Int, Int, Int)
findExpandPlace (x:xs) n = if or (map (not . singleton) (concat(concat x))) then (n,e,f,g) else findExpandPlace xs (n+1)
			where (e,f,g) = findMatrix x 0

expandExpandPlace :: NonDetermBoard a -> (Int, Int, Int, Int) -> [NonDetermBoard a]
expandExpandPlace = undefined
			
--------------------------------------------------------------------------			
-- GUESS
-- of type NonDetermBoard -> NonDetermBoard
-- do we need to take the head of zs?
--------------------------------------------------------------------------{			
{--guess k = if(finished zs) then zs else (guess zs)
		where
			do
				xs <- findExpandPlace k
				ys <- filter removedInvalid (expandBoard xs)
				zs <- head ys:tail ys
			return zs--}
			
-- a board is finished when it is full of singletons
--------------------------------------------------------------------------
-- Test case:
--   finished [[[[[1],[2]],[[3],[4]]],[[[3],[4]],[[2],[1]]]],[[[[4],[3]],[[2],[1]]],[[[1],[2]],[[4],[3]]]]]
--   expected output: True
--------------------------------
--   finished finished [[[[[1,2,3,4],[1,2,3,4]],[[1,2,3,4],[1,2,3,4]]],[[[4],[3]],[[2],[1]]]],[[[[2],[1]],[[4],[3]]],[[[1,2,3,4],[4]],[[1],[2]]]]]
--   expected output: False			
--------------------------------------------------------------------------						
finished :: NonDetermBoard a -> Bool
finished board = and(map(singleton) (concat(concat(concat board)))) 

-- this function filters all invalid boards, that is, boards which
-- contain values outside of 1..n^2.
removeInvalid :: [NonDetermBoard Int] ->[NonDetermBoard Int]
removeInvalid b = filter valid b

--------------------------------------------------------------------------
-- BOARD VALIDATION
--------------------------------------------------------------------------
-- this function takes in the size of the board and either rows, columns, or boxes
-- it will look at that list to check if it contains values that are not withing
-- [1..boardsize^2] and return false if there are.
--------------------------------------------------------------------------	
-- Test case:
--    allVal 2 [[50,5,3,4],[33,21,2,1],[4,3,1,2],[2,1,4,3]]
--    expected output: False  					
--------------------------------
--    allVal 2 [[1,2,3,4],[3,4,2,1],[4,3,1,2],[2,1,4,3]]
--    expected output: True
--------------------------------------------------------------------------						
allVal :: Int -> [[Int]] -> Bool
allVal n xs = ((foldr union [] xs) \\ values n) == []

-- given the board and its size, it removes the possible values elements, leaving only impossible values
convertPossToImposs :: Int -> NonDetermBoard Int -> NonDetermBoard Int
convertPossToImposs n = mapSud (\pos -> [1 .. (n^2)] \\ pos)

-- this is very shitty looking
-- checks rows / columns / boxes to see if it has any elements are not within 1..boardsize
validRows, validColumns, validBoxes :: NonDetermBoard Int -> Bool
validRows b =  foldr (&&) True (map (allVal (getBdSize(b))) (rows(b)))
validColumns b =  foldr (&&) True (map (allVal (getBdSize(b))) (columns(b)))
validBoxes b =  foldr (&&) True (map (allVal (getBdSize(b))) (boxes(b)))

-- Board is valid if it has all numbers [1..n^2]
--------------------------------------------------------------------------
-- Test case:
--   valid (populate [[[[1,3],[2,4]],[[4,3],[2,1]]],[[[2,1],[4,3]],[[3,4],[1,2]]]])
--   expected output: False
--------------------------------
--   valid (populate [[[[1,2],[3,4]],[[3,4],[2,1]]],[[[4,3],[2,1]],[[1,2],[4,3]]]])
--   expected output: True
--------------------------------------------------------------------------
valid :: NonDetermBoard Int -> Bool
valid board = validRows board && validColumns board && validBoxes board

-- an invalid board is a board that is not valid
invalid :: NonDetermBoard Int -> Bool
invalid = not . valid 

--------------------------------------------------------------------------
-- SOLVING SUDOKU
--------------------------------------------------------------------------
-- choices takes in a board and a number of the current element
-- it checks if it is a 0, then it will fill it in with a sublist of all possible values
-- otherwise, it leaves it there
-- this is ugly code because I'm only passing in a board to get the size of it 
-- to generate a list of possible values for that element
--------------------------------------------------------------------------
-- Test case:
--   choices [[[[1,2],[3,4]],[[4,3],[2,1]]],[[[2,1],[4,3]],[[3,4],[1,2]]]] 4
--   expected output: [4]
--------------------------------
--   choices [[[[1,2],[3,4]],[[4,3],[2,1]]],[[[2,1],[4,3]],[[3,4],[1,2]]]] 0
--   expected output: [1,2,3,4]
--------------------------------------------------------------------------
choices :: SudokuBoard a -> Int ->[Int]
choices board num 	| empty num = values (getBdSize board)
					| otherwise = [num]
		
-- function will populate the board full of possible values -- more ugly code as a result of the function 'choices'
--------------------------------------------------------------------------
-- Test case:
-- populate [[[[0,0],[0,0]],[[4,3],[2,1]]],[[[2,1],[4,3]],[[0,4],[1,2]]]]
-- expected output: [[[[[1,2,3,4],[1,2,3,4]],[[1,2,3,4],[1,2,3,4]]],[[[4],[3]],[[2],[1]]]],[[[[2],[1]],[[4],[3]]],[[[1,2,3,4],[4]],[[1],[2]]]]]
--------------------------------------------------------------------------
populate :: SudokuBoard Int -> NonDetermBoard Int
populate board = mapSud (choices board) board

solve :: Int -> NonDetermBoard a -> Maybe (SudokuBoard a, Int)
solve = undefined