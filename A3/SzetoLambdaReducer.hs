-- Lambda Calculus Reducer
-- CPSC521 Assignment 3
-- Derek Szeto
-- 315083
-------------------------------------------------------------------------------
-- Based off of Sean Nichols' example:
-- http://pages.cpsc.ucalgary.ca/~nicholss/ta/f06_417/417ex05.html
-------------------------------------------------------------------------------

module SzetoLambdaReducer where

import List

-- Declaring datatype for Lambda
-- A lambda term can be a variable, Absttraction, or Applylication	
data Lambda a = Var a
			  | Abst a (Lambda a)
			  | Apply (Lambda a) (Lambda a)

-------------------------------------------------------------------------------
-- Printing the Lambda term
-------------------------------------------------------------------------------
instance (Show a) => Show (Lambda a) where
	show (Var a) = show a
	show (Abst a l) = "\\" ++ (show a) ++ "." ++ (show l) ++ ""
	show (Apply l1 l2) = "(" ++ (show l1) ++ " " ++ (show l2) ++")"
	
--------------------------------------------------------------------------------
-- Determining free variables in a term 
-- Free variable cases
-- free(x) = {x}
-- free(\x.t) = free(t) - {x}
-- free(t1 t2) = free(t1) U free(t2)
--------------------------------------------------------------------------------
-- TEST CASE(S):
---------------------------------------------------------------
-- freevars (Var 1)
-- Expected Output: [1]
---------------------------------------------------------------
-- for (\x.x)
-- freevars (Abst 1 (Var 1))
-- Expected Output: []
---------------------------------------------------------------
-- for (\x.y)(x)
-- freevars (Apply (Abst 1 (Var 2)) (Var 1))
-- Expected Output: [2, 1]
---------------------------------------------------------------
-- for (\x.yx)(\z.z)
-- freevars (Apply (Abst 1 (Apply (Var 2) (Var 1))) (Abst 3 (Var 3)))
-- Expected Output: [2]
--------------------------------------------------------------------------------
freevars :: (Eq a) => (Lambda a) -> [a]
freevars (Var a) = [a]
freevars (Abst a t) = (freevars t) \\ [a]
freevars (Apply t1 t2) = union (freevars t1) (freevars t2)

--------------------------------------------------------------------------------
-- Getting fresh variables when performing an alpha conversion
-- Given a list of free variables, we need to pick one that is not in that list
-- In this case, just give the successor of the highest value in the given list
--------------------------------------------------------------------------------
-- TEST CASE(S):
---------------------------------------------------------------
-- freshvar [2]
-- Expected Output: 3
---------------------------------------------------------------
-- freshvar [2,4,1,3]
-- Expected Output: 5
--------------------------------------------------------------------------------
freshvar :: (Enum a, Ord a) => [a] -> a
freshvar xs = succ $ head $ reverse $ sort xs

--------------------------------------------------------------------------------
-- Deterining alpha-equivalence
-- Cases M =α N
-- x =α x
-- (\x.t) =α (\x'.t')   = t =α t' and x == x'
--					    = (\f.t[f/x] =α (\f.t'[f/x']) where f is a free variable
-- (t1 t2) =α (t1' t2') = t1 =α t2 and t2 α= t2'
--------------------------------------------------------------------------------
-- TEST CASE(S):
---------------------------------------------------------------
-- alphaEquiv (Var 1) (Var 1)
-- Expected Output: True
---------------------------------------------------------------
-- alphaEquiv (Var 1) (Var 2)
-- Expected Output: False
---------------------------------------------------------------
-- alphaEquiv (Abst 1 (Var 3)) (Abst 1 (Var 2))
-- Expected Output: False
--------------------------------------------------------------------------------
alphaEquiv :: (Enum a, Ord a) => Lambda a -> Lambda a -> Bool
alphaEquiv (Var x) (Var x') 			= x == x'
alphaEquiv (Abst x t) (Abst x' t')		| x == x'   = alphaEquiv t t'
										| otherwise = alphaEquiv (subst t (f, x)) (subst t' (f, x')) where
											f = Var $ freshvar ([x, x'] ++ (freevars t) ++ (freevars t'))
alphaEquiv (Apply t1 t2) (Apply t1' t2')	= (alphaEquiv t1 t1') && (alphaEquiv t2 t2')

--------------------------------------------------------------------------------
-- Performing lambda subsitution
-- Substitution cases
-- x[L/y]       = L if x==y
--              = x otherwise
-- (\x.t)[L/y]  = (\x.t) if x==y
--			    = (\x.t) if y not elem of t
--			    = (\f.(t[f/x])[L/y]) if x is elem of L, f = new fresh variable
--			    = (\x.t[L/y]) otherwise
-- (t1 t2)[L/y] = (t1[L/y] t2[L/y])
--------------------------------------------------------------------------------
-- TEST CASE(S):
---------------------------------------------------------------
-- subst (Var 1) ((Var 2), 2)
-- Expected Output: 1
---------------------------------------------------------------
-- subst (Var 1) ((Var 2), 1)
-- Expected Output: 2
---------------------------------------------------------------
-- subst (Abst 3 (Var 1)) ((Var 2), 3)
-- Expected Output: \3.1
---------------------------------------------------------------
-- subst (Abst 3 (Var 1)) ((Var 2), 2)
-- Expected Output: \3.1
---------------------------------------------------------------
-- variable capture ( \x.y [x/y])
-- subst (Abst 1 (Var 2)) ((Var 1), 2)
-- Expected Output: \3.1
---------------------------------------------------------------
-- subst (Abst 1 (Var 2)) ((Var 5), 2)
-- Expected Output: \1.5
---------------------------------------------------------------
-- (\x.y)(\x.y)[w/y]
-- subst (Apply (Abst 1 (Var 2)) (Abst 1 (Var 2)) ) ((Var 4), 2)
-- Expected Output: \1.4\1.4
--------------------------------------------------------------------------------
subst :: (Enum a, Ord a) => Lambda a -> (Lambda a, a) -> Lambda a
subst (Var x) (l, y)
	| x == y						= l
	| otherwise						= Var x
subst (Abst x t) (l, y)
	| x == y						= Abst x t
	| not (elem y (freevars t))		= Abst x t
	| elem x (freevars l)			= Abst f (subst (subst t ((Var f), x)) (l, y))
	| otherwise						= Abst x (subst t (l, y)) where
		f = freshvar (freevars t)
subst (Apply t1 t2) (l,y)				= Apply (subst t1 (l, y)) (subst t2 (l, y))

--------------------------------------------------------------------------------
-- performs one step of By-Name Reduction
-- The boolean tells us if this lambda term can be reduced further. 
-- True if it can, False if it cannot
--------------------------------------------------------------------------------
-- TEST CASE(S):
---------------------------------------------------------------
-- byNameReduc ((Abst 1 (Var 1)), False)
-- expected output: (\1.1, False)
--------------------------------------------------------------------------------
byNameReduc :: (Enum a, Ord a) => (Lambda a, Bool) -> (Lambda a, Bool)
byNameReduc (lambda, True)                         = (lambda, True)
byNameReduc (Var a, False)                	  = (Var a, False)
byNameReduc (Apply (Abst a t1) t2, False)     = (subst t1 (t2, a), True)   -- (x.N)M form, this is the next redex
byNameReduc (Apply t1 t2, False) | b1         = (Apply s1 t2, True)        -- M N, M is not abst; find the redex for it
                                 | otherwise  = (Apply t1 s2, b2) where	 -- no? how sad, lets try it for N
    (s1, b1) = byNameReduc (t1, False)
    (s2, b2) = byNameReduc (t2, False)
byNameReduc (Abst a t, False)                 = (Abst a s, b) where
    (s, b)   = byNameReduc (t, False)
byNameReduc (lambda, False)                        = (lambda, True)

--------------------------------------------------------------------------------
-- performs one step of a Head Reduction
-- The boolean tells us if this lambda term can be reduced further. 
-- True if it can, False if it cannot
--------------------------------------------------------------------------------
-- TEST CASE(S):
---------------------------------------------------------------
-- headReduc ((Abst 1 (Var 1)), False)
-- expected output: (\1.1, False)
--------------------------------------------------------------------------------	
headReduc :: (Enum a, Ord a) => (Lambda a, Bool) -> (Lambda a, Bool)
headReduc (lambda, True)				= (lambda, True)
headReduc (Var a, False)				= (Var a, False)
headReduc (Apply (Abst a t1) t2, False) = (subst t1 (t2, a), True) -- (x.N)M form - this is the next redex
headReduc (Apply t1 t2, False)
	| b1								= (Apply s1 t2, True) -- M N, M is not abst; find the redex for it
	| otherwise							= (Apply t1 t2, False)  where -- otherwise... fail to find it
		(s1, b1) = headReduc (t1, False)
headReduc (Abst a t, False) 			= (Abst a s, b) where
	(s, b) = headReduc (t, False)
headReduc (lambda, False)				= (lambda, True)
	
--------------------------------------------------------------------------------
-- performs one step of a By-Value Reduction
-- The boolean tells us if this lambda term can be reduced further. 
-- True if it can, False if it cannot
--------------------------------------------------------------------------------
-- TEST CASE(S):
---------------------------------------------------------------
-- byValueReduc ((Abst 1 (Var 1)), False)
-- expected output: (\1.1, False)
--------------------------------------------------------------------------------
byValueReduc :: (Enum a, Ord a) => (Lambda a, Bool) -> (Lambda a, Bool)
byValueReduc (lambda, True)					= (lambda, True)
byValueReduc (Var a, False)					= (Var a, False)
byValueReduc (Apply (Abst a t1) t2, False)
 	| b2									= (Apply (Abst a t1) s2, True) -- (x.M)N, find redex for N
	| otherwise								= (Apply (Abst a s1) t2, True) where -- no for N? do it for M (innermost)
		(s1, b1) = byValueReduc (t1, False)
		(s2, b2) = byValueReduc (t2, False)
byValueReduc (Abst a t, False)				= (Abst a s, b) where
	(s, b) = byValueReduc (t, False)
byValueReduc (lambda, False)				= (lambda, True)

--------------------------------------------------------------------------------
-- Returns the normal form as a result of by-name reduction
-- if it can be reduced, it will recurse on the term returned by one-step function
--------------------------------------------------------------------------------
-- TEST CASE(S): -- see testcases.txt for what each case is defined as
---------------------------------------------------------------
-- byNameNormalize app
-- expected output: \1.1
---------------------------------------------------------------
-- byNameNormalize omega
-- expected output: INFINITE LOOP
---------------------------------------------------------------
-- byNameNormalize cond					-- conditionally terminating expressions
-- expected output: \1.11
---------------------------------------------------------------
-- byNameNormalize add					-- adding 2 3
-- expected output: \3.\4.(3 (3 (3 (3 (3 4)))))
---------------------------------------------------------------
-- byNameNormalize append				-- append [2,3] [4,5]
-- expected output: \3.\4.((3 \6.\7.(6 (6 7))) ((3 \6.\7.(6 (6 (6 7)))) ((3 \6.\7.(6 (6 (6 (6 7))))) ((3 \6.\7.(6 (6 (6 (6 (6 7)))))) 4))))
---------------------------------------------------------------
-- byNameNormalize factorial				-- factorial of 4
-- expected output: \1.\2.(1 (1 (1 (1 (1 (1 (1 (1 (1 (1 (1 (1 (1 (1 (1 (1 (1 (1 (1 (1 (1 (1 (1 (1 2))))))))))))))))))))))))
--------------------------------------------------------------------------------
byNameNormalize :: (Enum a, Ord a) => Lambda a -> Lambda a
byNameNormalize lambda | canReduce = byNameNormalize newLambda 
				  	   | otherwise = newLambda where
					(newLambda, canReduce) = byNameReduc (lambda, False)
					
--------------------------------------------------------------------------------
-- Returns head normal form as a result of head-reduction
-- works same as byNameNormalize
--------------------------------------------------------------------------------
-- TEST CASE(S): -- see testcases.txt for what each case is defined as
---------------------------------------------------------------
-- headNormalize app
-- expected output: \1.1
---------------------------------------------------------------
-- headNormalize omega
-- expected output: INFINITE LOOP
---------------------------------------------------------------
-- headNormalize cond					-- conditionally terminating expressions
-- expected output: \1.11
---------------------------------------------------------------
-- headNormalize add					-- adding 2 3
-- expected output: \3.\4.(3 (3 (3 (3 (3 4)))))
---------------------------------------------------------------
-- headNormalize append				-- append [2,3] [4,5]
-- expected output: \3.\4.((3 \6.\7.(6 (6 7))) ((3 \6.\7.(6 (6 (6 7)))) ((3 \6.\7.(6 (6 (6 (6 7))))) ((3 \6.\7.(6 (6 (6 (6 (6 7)))))) 4))))
---------------------------------------------------------------
-- headNormalize factorial				-- factorial of 4
-- uhh something went wrong here
--------------------------------------------------------------------------------
headNormalize :: (Enum a, Ord a) => Lambda a -> Lambda a
headNormalize lambda | canReduce = headNormalize newLambda 
				     | otherwise = newLambda where
					(newLambda, canReduce) = headReduc (lambda, False)
					
--------------------------------------------------------------------------------
-- Returns normal form as a result of by-value reduction
-- works same as byNameNormalize
--------------------------------------------------------------------------------
-- TEST CASE(S): -- see testcases.txt for what each case is defined as
---------------------------------------------------------------
-- headNormalize app
-- expected output: \1.1
---------------------------------------------------------------
-- headNormalize omega
-- expected output: INFINITE LOOP
---------------------------------------------------------------
-- headNormalize cond					-- conditionally terminating expressions
-- expected output: INFINITE LOOP
---------------------------------------------------------------
-- headNormalize add					-- adding 2 3
-- expected output: \3.\4.(3 (3 (3 (3 (3 4)))))
---------------------------------------------------------------
-- headNormalize append				-- append [2,3] [4,5]
-- expected output: \3.\4.((3 \6.\7.(6 (6 7))) ((3 \6.\7.(6 (6 (6 7)))) ((3 \6.\7.(6 (6 (6 (6 7))))) ((3 \6.\7.(6 (6 (6 (6 (6 7)))))) 4))))
---------------------------------------------------------------
-- byValue factorial				-- factorial of 4
-- expected output: INFINITE LOOP
--------------------------------------------------------------------------------
byValueNormalize :: (Enum a, Ord a) => Lambda a -> Lambda a
byValueNormalize lambda | canReduce = byValueNormalize newLambda
						| otherwise = newLambda where
					(newLambda, canReduce) = byValueReduc (lambda, False)
