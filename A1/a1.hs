{-
Derek Szeto
315083
CPSC521 Assignment 1

Collaborated Derek Corkill and David Pusch
-}

module Assignment1 where

{--------------------------------
	QUESTION 1 - app and rev
--------------------------------}
-- Test cases: ([1,2,3], [1,2]) --> [1,2,3,1,2]
--			   ([], [1,2]) --> [1,2]
--             ([1,2], []) --. [1,2]

app::([a],[a])->[a]
app ([],ys) = ys
app (xs,[]) = xs
app (xs, ys) = xs ++ ys

-- foldr version
app2::([a],[a])->[a]
app2 (xs,ys) = foldr (:) ys xs

--REV
-- Test cases: [] --> []
--			   [1,2,3] --> [3,2,1]

rev::[a]->[a]
rev [] = []
rev (x:xs) = rev xs ++ [x]

rev2::[a]->[a]
rev2 xs = foldl (flip(:)) [] xs

{--------------------------------
	QUESTION 2 - flatten
--------------------------------}
-- test cases: flatten [[]] = []
--		       flatten [[1],[2,3],4,[5,6]] = [1,2,3,4,5,6]
-- this fails if the list gets too deep (ie. [1,[2,[3]]])

flatten::[[a]]->[a]
flatten [[]] = []
flatten (x:xs) = x ++ flatten xs

flatten2::[[a]]->[a]
flatten2 xs = foldr (++) [] xs

{--------------------------------
	QUESTION 4 - lexInt
--------------------------------}
--test cases: lexInt [] [] --> True
--			  lexInt [] [2] --> False
-- 			  lexInt [1] [] --> True
--		      lexInt [1,2] [3,4] --> False
--			  lexINt [2,3] [2,2] -> True

lexInt::[Int]->[Int]->Bool
lexInt [] [] = True
lexInt [] ys = False
lexInt xs [] = True
lexInt (x:xs) (y:ys) | x>=y = lexInt xs ys
			         | otherwise = False
			
--ord a
lexInt2::(Ord a)=>[a]->[a]->Bool
lexInt2 [] [] = True
lexInt2 [] ys = False
lexInt2 xs [] = True
lexInt2 (x:xs) (y:ys) | x>=y = lexInt2 xs ys
			         | otherwise = False
			
-- modifies show
lexInt3::[Int]->[Int]->Bool
lexInt3 [] [] = True
lexInt3 [] ys = False
lexInt3 xs [] = True
lexInt3 (x:xs) (y:ys) | show x >= show y = lexInt3 xs ys
			          | otherwise = False


{--------------------------------
	QUESTION 9 - quickSort
--------------------------------}
-- from in class
-- test case: quicksort [1,4,2,35,8,0,11] --> [0,1,2,4,8,11,35]
quicksort::(Ord a)=>[a]->[a]
quicksort [] = []
quicksort (x:xs) = (quicksort [less|less<-xs, less<x]) ++ [x] ++ (quicksort [more|more<-xs, more>=x])

--same but done with filters
quicksort2::(Ord a)=>[a]->[a]
quicksort2 [] = []
quicksort2 (x:xs) = quicksort(less) ++ [x] ++ quicksort(more) where 
					less = (filter (<x) xs)
					more = (filter (>=x) xs)
{--------------------------------
	QUESTION 11 - relgrp
--------------------------------}
-- this was discussed in the tutorial and is on Jon's site

--greaterthan relation
greaterThan::Int->Int->Bool
greaterThan xs 0 = True
greaterThan x y = if (x<=y) then True else False

relgrp :: (a -> b -> Bool) -> [a] -> [b] -> [(a,[b])]
relgrp rel [] _ = []
relgrp rel (x:xs) ys = (x, filter (rel x) ys) : relgrp rel xs ys

{--------------------------------
	QUESTION 12 - group
--------------------------------}
-- test cases: nbr 1 2 --> True
--			   nbr 2 1 --> True
--			   nbr 2 2 --> True
-- 			   nbr 4 6 --> False


nbr::Int->Int->Bool
nbr x y = if ((x==y)||(x==(y-1)||(x==(y+1)))) then True else False

--adds an element into the list
-- test cases:	addToList 1 [[2,3],[5,6]] --> [[1,2,3],[5,6]]
addToList::a->[[a]]->[[a]]
addToList x [] = [[x]]
addToList x (y:ys) = (x:y):ys

--test cases: group nbr [3,4,7,8,10,20,21,30] --> [[3,4],[7,8],[20,21],[30]]
-- 			  group nbr [3] -->[[3]]
--		      group nbr [] = [[]]
group::(a->a->Bool)->[a]->[[a]]
group predicate [] = [[]]
group predicate [x] = [[x]]
group predicate (x:y:stuff) = if (predicate x y ) then addToList x (group predicate (y:stuff))
							  else [x]:(group predicate (y:stuff))

{--------------------------------
	QUESTION 13 - subsets
--------------------------------}
-- test cases: subsets [] --> [[]]
--			   subsets [1,2,3] --> [[],[1],[2],[3],[2,3],[1,3],[1,2],[1,2,3]]
subsets::[a]->[[a]]
subsets [] = [[]]
subsets (x:xs) =  subsets xs ++ map (x:) (subsets xs)

{--------------------------------
	QUESTION 14 - perm
--------------------------------}
--test cases: perm [1,2,3] --> [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
-- 			  perm [] = [[]]
--derived from http://davidtran.doublegifts.com/blog/?p=5
perm::(Eq a)=>[a]->[[a]]
perm [] = [[]]
perm xs = [x:ys| x<-xs, ys<-perm (filter (/=x) xs)]

{--------------------------------
	QUESTION 17 - addpoly & multpoly
--------------------------------}
-- Test Cases:   addPoly [1,2] [] --> [1.0.2.0]
--				 addPoly [] [3,4] --> [3.0,4..0]
--				 addPoly [1,2,3,4], [1,2] --> [2.0,4.0,3.0,4.0]
--               multPoly [1,2,3,4], [1,2] --> [1.0,4.0,3.0,4.0]

addPoly::[Float]->[Float]->[Float]
addPoly [] ys = ys
addPoly xs [] = xs
addPoly (x:xs) (y:ys) = x+y : addPoly xs ys

-- uhh I know this is totally wrong.
-- Idea: Multiply each element from the 2nd list with the first poly. Then add them all up.
-- Shift?
--multPoly::[Float]->[Float]->[Float]
--multPoly (x:xs) (y:ys) = foldr addPoly x y []?

multPoly::[Float]->[Float]->[Float]
multPoly [] ys = ys
multPoly xs [] = xs
multPoly (x:xs) (y:ys) = x*y : addPoly xs ys

{--------------------------------
	QUESTION 21 - fac
--------------------------------}
--takes the factorial
-- test case: fac 1891 --> some big ass number (but it is a number)
-- the arbirary precision type Integer allows us to calculate such a high number
-- algorithm from: http://www.willamette.edu/%7Efruehr/haskell/evolution.html

fac::Integer->Integer
fac n = foldl (*) 1 [1..n]